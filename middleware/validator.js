function validateBook(req,res,next) {
	var validator = require('jsonschema').Validator;
    var v = new validator();

    var schema = {
    "id": "/book",
    "type": "object",
    "properties": {
      "title": {"type": "string", "minLength" : 4},
      "uploadFile": {"type": "file"}
	},
	"required": ["title", "author"]
	};

	try{
		v.validate(req.body, schema, {throwError: true});
		next();
	}
	catch(e)
	{
    return res.status(400).send(e.message);
	}
};
  
  module.exports.validate = validateBook;