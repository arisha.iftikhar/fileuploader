var fs = require("fs");
var express = require("express");
const exphbs = require("express-handlebars");

const path = require("path");

var app = express();

// Handlebars Middleware for Frontend
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

app.get("/", (req, res) => {
  res.render("index", {
    title: "File Uploader",
  });
});

var Busboy = require("busboy");

app.post("/api/upload", function(req, res) {
  var busboy = new Busboy({ headers: req.headers });
  if (busboy) {
    busboy.on("file", function(fieldname, file, filename, encoding, mimetype) {
      var saveTo = path.join(__dirname, "Uploads", filename);
      file.pipe(fs.createWriteStream(saveTo));
    });

    busboy.on("finish", function() {
      res.writeHead(200, { Connection: "close" });
      res.end("File uploaded");
    });
    req.pipe(busboy);
  }
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
